# https://open.kattis.com/problems/aaah

jonAaah = input()
doctorAaah = input()

if (len(jonAaah) - 1) < (len(doctorAaah) - 1):
    print("no")
else:
    print("go")
