# https://open.kattis.com/problems/r2

data = input()
values = data.split(" ")

print((int(values[1]) * 2) - int(values[0]))
